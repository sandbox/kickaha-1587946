<?php

/**
 * Implements hook_rules_action_info().
 */
function commerce_easypay_cc_rules_action_info() {
  $actions = array();
  $actions['commerce_easypay_cc_charge_action'] = array(
    'label' => t('Charge Credit card'),
    'group' => t('Commerce Payment'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order', array(), array('context' => 'a drupal commerce order')),
        'skip save' => TRUE,
      ),
    ),
  );
  return $actions;
}

/**
 * Rules action to charge a credit card transaction.
 */
function commerce_easypay_cc_charge_action($order) {
  if ($order->status != 'completed' || $order->original->status != 'processing') {
    return;
  }
  $transactions = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order->order_id, 'payment_method' => 'commerce_easypay_cc', 'remote_status' => COMMERCE_EASYPAY_CC_STATUS_AUTHORIZED));
  foreach ($transactions as $transaction) {
    commerce_easypay_cc_charge($order, $transaction, FALSE);
  }
}
