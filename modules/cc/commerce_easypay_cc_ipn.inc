<?php
/**
 * @file
 * Page callbacks for returning payment details & payment returns for easypay credit card payment.
 */
function commerce_easypay_cc_details() {

  $ref = new stdClass();
  $ref->entity = $_GET['e'];
  $ref->reference = $_GET['r'];
  $ref->value = $_GET['v'];
  $ref->country = $_GET['c'];
  $ref->language = $_GET['l'];
  $ref->t_key = $_GET['t_key'];
  $order = NULL;
  $order_wrapper = NULL;

  watchdog('commerce_easypay', 'Incoming Order Details Request');

  if (empty($ref->entity) || empty($ref->reference) || empty($ref->value)) {
    watchdog('commerce_easypay', 'Missing arguments in details request');
    commerce_easypay_cc_details_error($ref, 'ep_entity or ep_reference or ep_value not ok;');
    return;
  }

  if (empty($ref->t_key)) {
    watchdog('commerce_easypay', 'Missing t_key in request');
    commerce_easypay_cc_details_error($ref, 't_key not ok;');
  }

  $order = commerce_order_load_by_number($ref->t_key);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  if (!$order || !$order_wrapper) {
    watchdog('commerce_easypay', 'Invalid Order ID (@order_id)', array('@order_id' => $ref->t_key));
    commerce_easypay_cc_details_error($ref, 't_key not ok;');
    return;
  }

  if (!commerce_easypay_request_from_valid_ip()) {
    commerce_easypay_cc_details_error($ref, 'validation by ip; ip not ok;');
    return;
  }

  $billing = commerce_easypay_cc_details_billing_info($order_wrapper);
  $shipping = commerce_easypay_cc_details_shipping_info($order_wrapper);
  $items = commerce_easypay_cc_details_order_details($order_wrapper);

  commerce_easypay_cc_details_success($ref, $order_wrapper, $billing, $shipping, $items);
}

/**
 * Page callback handler for the easypay visa-fwd url.
 * Load redirect key for the given order and resume the normal
 * commerce checkout process.
 */
function commerce_easypay_cc_fwd() {

  $order = NULL;
  $data = array();

  $data['e'] = $_GET['e'];
  $data['r'] = $_GET['r'];
  $data['v'] = $_GET['v'];
  $data['k'] = $_GET['k'];
  $data['s'] = $_GET['s'];
  $data['t_key'] = $_GET['t_tkey'];

  $order_id = $_GET['t_key'];
  $k = $_GET['k'];

  if ($order_id) {
    $order = commerce_order_load_by_number($order_id);
    $order->data['commerce_easypay']['auth_status'] = $data['s'];
    $order->data['commerce_easypay']['auth_key'] = $data['k'];
    commerce_order_save($order);

    $url = url('checkout/' . $order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
    header('Location: ' . $url);
    flush();
    exit(0);
  }
}

/**
 * Returns Array with key-value pairs to be output as part of the XML response.
 */
function commerce_easypay_cc_details_order_details(&$order_wrapper) {
  $items = array();
  foreach ($order_wrapper->commerce_line_items as $item) {
    if ($item->type->value() == 'product') {
      $items[] = array(
        'item_description' => $item->line_item_label->value(),
        'item_quantity'    => $item->quantity->value(),
        'item_total'       => commerce_currency_amount_to_decimal($item->commerce_total->amount->value(), $item->commerce_total->currency_code->value()),
      );
    }
  }

  return $items;
}

/**
 * Returns Array with key-value pairs to be output as part of the XML response.
 */
function commerce_easypay_cc_details_billing_info(&$order_wrapper) {
  $billing = array();
  $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address;
  $billing['bill_name'] = $billing_address->name_line->value();
  $billing['bill_address_1'] = $billing_address->thoroughfare->value();
  $billing['bill_address_2'] = $billing_address->premise->value();
  $billing['bill_city'] = $billing_address->locality->value();
  $billing['bill_zip_code'] = $billing_address->postal_code->value();
  $billing['bill_country'] = $billing_address->country->label();

  return $billing;
}

/**
 * Returns Array with key-value pairs to be output as part of the XML response.
 */
function commerce_easypay_cc_details_shipping_info(&$order_wrapper) {
  $shipping = array();
  $shipping_address = $order_wrapper->commerce_customer_shipping->commerce_customer_address;
  $shipping['shipp_name'] = $shipping_address->name_line->value();
  $shipping['shipp_address_1'] = $shipping_address->thoroughfare->value();
  $shipping['shipp_address_2'] = $shipping_address->premise->value();
  $shipping['shipp_city'] = $shipping_address->locality->value();
  $shipping['shipp_zip_code'] = $shipping_address->postal_code->value();
  $shipping['shipp_country']= $shipping_address->country->label();

  return $shipping;
}

/**
 * Success XML response for CC payments Order Details request from easypay.
 */
function commerce_easypay_cc_details_success($ref, &$order_wrapper, $billing, $shipping, $items) {
  header('Content-Type: text/plain');
  $r ='';
  $r ='<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";
  $r .= '<get_detail>' . "\n";
  $r .= '<ep_status>ok</ep_status>' . "\n";
  $r .= '<ep_message>ok;</ep_message>' . "\n";
  $r .= '<ep_entity>' . $ref->entity . '</ep_entity>' . "\n";
  $r .= '<ep_reference>' . $ref->reference . '</ep_reference>' . "\n";
  $r .= '<ep_value>' . $ref->value . '</ep_value>' . "\n";
  $r .= '<t_key>' . $ref->t_key . '</t_key>' . "\n";

  // Add Order Billing information
  if (!empty($billing) || !empty($shipping)) {
    $r .= '<order_info>' . "\n";
    foreach ($billing as $key => $value) {
      $r .= '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
    }
    foreach ($shipping as $key => $value) {
      $r .= '<' . $key . '>' . $value . '</' . $key . '>' . "\n"; 
    }
    $r .= '</order_info>' . "\n";
  }

  // Add Order items information
  if (!empty($items)) {
    $r .= '<order_detail>' . "\n";
    foreach ($items as $item) {
      $r .= '<item>' . "\n";
      foreach ($item as $key => $value) {
        $r .= '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
      }
      $r .= '</item>' . "\n";
    }
    $r .= '</order_detail>' . "\n";
  }
  $r .= '</get_detail>' . "\n";

  echo mb_convert_encoding($r, 'ISO-8859-1');
  flush();
  drupal_exit();
}

/**
 * Returns an xml error response to the payment details request from easypay.
 */
function commerce_easypay_cc_details_error($ref, $message) {
  header('Content-Type: text/plain');
  $r ='';
  $r ='<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";
  $r .= '<get_detail>' . "\n";
  $r .= '<ep_status>err</ep_status>' . "\n";
  $r .= '<ep_message>' . $message . '</ep_message>' . "\n";
  $r .= '<ep_entity>' . $ref->entity . '</ep_entity>' . "\n";
  $r .= '<ep_reference>' . $ref->reference . '</ep_reference>' . "\n";
  $r .= '<ep_value>' . $ref->value . '</ep_value>' . "\n";
  $r .= '</get_detail>' . "\n";
  echo mb_convert_encoding($r, 'ISO-8859-1');
  flush();
  drupal_exit();
}
