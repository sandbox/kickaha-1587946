<?php
/**
 * @file
 * This file includes IPN handling callback page processing.
 */

/**
 * Process easypay IPN key request
 *
 * Check that the IPN key request from easypay is valid
 * return valid/invalid XML response accordingly.
 */
function commerce_easypay_mb_process_ipn() {  

  $ref = new stdClass();
  $ref->ep_cin  = $_GET['ep_cin'];
  $ref->ep_user = $_GET['ep_user'];
  $ref->ep_doc  = $_GET['ep_doc'];
  
  // Log an incoming validation request
  $message_vars = array('@cin' => $ref->ep_cin, '@user' => $ref->ep_user, '@doc' => $ref->ep_doc);
  watchdog('commerce_easypay', 'IPN key request (CIN: @cin, User: @user, DOC: @doc).', $message_vars);

  // Validate that all data is passed in
  if (empty($ref->ep_cin) || empty($ref->ep_user) || empty($ref->ep_doc)) {
    watchdog('commerce_easypay', 'ep_cin or ep_user or doc and key not ok;');
    commerce_easypay_mb_ipn_error($ref, $message);
    drupal_exit();
  }

  // Get ep_key for matching ep_doc
  $query = 'SELECT cep.ep_key FROM {commerce_easypay} AS cep WHERE cep.ep_doc = :ep_doc';
  $query_args = array(':ep_doc' => $ref->ep_doc);
  $result = db_query($query, $query_args);
  $ref->ep_key = $result->fetchColumn(0);
  
  // Write new entry if there was no previous ep_doc entry existed
  if (empty($ref->ep_key)) {
    $ref->ep_key = db_insert('commerce_easypay')
      ->fields(array(
        'ep_cin'  => $ref->ep_cin,
        'ep_user' => $ref->ep_user,
        'ep_doc'  => $ref->ep_doc,
      ))
      ->execute();
    if (empty($ref->ep_key)) {
      commerce_easypay_mb_ipn_error($ref, 'doc ' . $ref->ep_doc . ' and key ' . $ref->ep_key . ' not ok;' );
      drupal_exit();
    }
    watchdog('commerce_easypay', 'IPN key created for ep_doc @doc.', array('@doc' => $ref->ep_doc));
  }

  if (!commerce_easypay_request_from_valid_ip()) {
    $message = 'doc ' . $ref->ep_doc . ' and key ' . $ref->ep_key . ' ok;';
    $message .= 'validation by ip; ip not ok;';
    commerce_easypay_mb_ipn_error($ref, $message);
    exit(0);
  }

  commerce_easypay_mb_ipn_success($ref, 'doc ' . $ref->ep_doc . ' and key ' . $ref->ep_key . ' ok; validation by ip; ip ok;');
  drupal_exit(); 
}

/**
 * Returns an xml success notification key to the request from easypay.
 */
function commerce_easypay_mb_ipn_success($ref, $message = NULL) {
  header('Content-Type: text/plain');
  $r = '';
  $r = '<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";
  $r .= '<getautoMB_key>' . "\n";
  $r .= '<ep_status>ok0</ep_status>' . "\n";
  $r .= '<ep_message>' . $message . '</ep_message>' . "\n";
  $r .= '<ep_cin>' . $ref->ep_cin . '</ep_cin>' . "\n";
  $r .= '<ep_user>' . $ref->ep_user . '</ep_user>' . "\n";
  $r .= '<ep_doc>' . $ref->ep_doc . '</ep_doc>' . "\n";
  $r .= '<ep_key>' . $ref->ep_key . '</ep_key>' . "\n";
  $r .= '</getautoMB_key>' . "\n";
  echo mb_convert_encoding($r, 'ISO-8859-1');
  flush();
  drupal_exit();
}

/**
 * Returns an xml error response to the notification key request from easypay.
 */
function commerce_easypay_mb_ipn_error($ref, $message) {
  header('Content-Type: text/plain');
  $r ='';
  $r ='<?xml version="1.0" encoding="ISO-8859-1"?>' . "\n";
  $r .= '<getautoMB_key>' . "\n";
  $r .= '<ep_status>err</ep_status>' . "\n";
  $r .= '<ep_message>' . $message . '</ep_message>' . "\n";
  $r .= '<ep_cin>' . $ref->ep_cin . '</ep_cin>' . "\n";
  $r .= '<ep_user>' . $ref->ep_user . '</ep_user>' . "\n";
  $r .= '<ep_doc>' . $ref->ep_doc . '</ep_doc>' . "\n";
  $r .= '<ep_key></ep_key>' . "\n";
  $r .= '</getautoMB_key>' . "\n";
  echo mb_convert_encoding($r, 'ISO-8859-1');
  flush();
  drupal_exit();
}
