<?php
/**
 * @file
 * Builds placeholder replacement tokens for easypay CC payment data.
 */

/**
 * Implements hook_token_info().
 */
function commerce_easypay_mb_token_info() {
  $type = array(
    'name' => t('Orders payments', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Tokens related to payment transactions.'),
    'needs-data' => 'commerce-order',
  );

  // tokens for payments
  $payments = array();

  $payments['mb-entity'] = array(
    'name' => t('MB payment entity'),
    'description' => t('Entity generated for MB payment'),
  );
  $payments['mb-reference'] = array(
    'name' => t('MB payment reference'),
    'description' => t('Reference generated for MB payment'),
  );
  $payments['mb-value'] = array(
    'name' => t('MB payment reference'),
    'description' => t('Value for MB payment'),
  );
  
  return array(
    'types' => array('commerce-order' => $type),
    'tokens' => array('commerce-order' => $payments),
  ); 
}

/**
 * Implements hook_tokens().
 */
function commerce_easypay_mb_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];
    $payment_info = $order->data['commerce_easypay'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    
    // Avoid returning values for other payments types if same keys are set
    if (!isset($order->data['payment_method']) || drupal_substr($order->data['payment_method'], 0, drupal_strlen('commerce_easypay_mb')) != 'commerce_easypay_mb') {
      $payment_info = array('entity' => '', 'reference' => '', 'value' => '');
    }

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'mb-entity':
          $mb_entity = $payment_info['entity'];
          $replacements[$original] = $sanitize ? check_plain($mb_entity) : $mb_entity;
          break;
          
        case 'mb-reference':
          $mb_reference = $payment_info['reference'];
          $replacements[$original] = $sanitize ? check_plain($mb_reference) : $mb_reference;
          break;
          
        case 'mb-value':
          $mb_value = $payment_info['value'] . ' ' . ($payment_info['value'] ? $order_wrapper->commerce_order_total->currency_code->value() : '');
          $replacements[$original] = $sanitize ? check_plain($mb_value) : $mb_value;
          break;
      }
    }
  }

  return $replacements;
}

